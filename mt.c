#include <omp.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> 

extern char *optarg;

int main(int argc, char* argv[]) {
    
    int samples = 1000000;
    int threads = 1;
    int opt;
    while( (opt = getopt(argc, argv, "hs:t:") ) != -1) 
    { 
        switch(opt) 
        { 
            case 's': 
                samples = atoi(optarg);
                break;
            case 't': 
                threads = atoi(optarg);
                break;
            case 'h':
                printf("$ 3_4_a -s {# of samples, default: 1000000} -t {# of threads, default: 1}\n");
        } 
    } 

    srand( time(NULL) );
 
    double exec_time;
    struct timeval time_start, time_end;    
    gettimeofday(&time_start, NULL);

    int i;
    float circle = 0;

    #pragma omp parallel for num_threads(threads)
    for (i = 0; i < samples; i++) {
        if ( pow(2.0f * ( (float) rand() / (float) RAND_MAX) - 1.0f, 2) + pow(2.0f * ( (float) rand() / (float) RAND_MAX) - 1.0f, 2) <= 1.0f) {
            circle += 1.0f;
        }
    }
    
    gettimeofday(&time_end, NULL);
    exec_time = (time_end.tv_sec - time_start.tv_sec) * 1000.0;
    exec_time += (time_end.tv_usec - time_start.tv_usec) / 1000.0;
    float approx_pi = ( 4.0f * circle / (float) samples);  // nach PI aufgelöst von PI / 4 = HITS IN CIRCLE / SAMPLE 

    printf("PI is approximately: %lf\n", approx_pi);
    printf("Execution time: %f ms\n -> %d thread(s) and %d samples used.\n", exec_time, threads, samples);
    printf(" -> Error: %f", fabs( M_PI - approx_pi) );
}