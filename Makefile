CFLAGS=-Wall -lomp -lm -fopenmp
PLATFORM_CFLAGS=-march=core2
CC=gcc -O 

mt: mt.c
	$(CC) $(CFLAGS) mt.c -o ../out/mt

.PHONY: clean

clean:
	rm -f mt
