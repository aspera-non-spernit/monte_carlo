/**
* ChatGPT refined code
* I: Look at this C-code: <paste code> What can be improved?
* ChatGPT:

Here are some potential improvements to the provided C code:

    Optimization Flags: Consider adding optimization flags to the compiler command to improve the performance of the code. For example, using -O3 with GCC can enable aggressive optimizations.
    Avoid Using rand(): The rand() function may not be the best choice for generating random numbers due to its limitations. Consider using a more robust random number generator, such as rand48() or the random functions provided by the C++11 <random> library.
    Better Random Number Generation: Instead of generating two random numbers separately for the x and y coordinates, consider using a more efficient method, such as generating a single random point within the unit square and checking if it falls within the unit circle.
    Improved Error Handling: Add error handling for invalid command-line arguments, such as negative sample sizes or thread counts.
    Consistent Formatting: Ensure consistent formatting throughout the code, such as indentation and spacing, to improve readability.
    Use OpenMP Reduction: Utilize OpenMP reduction clause to perform the reduction operation (circle += 1.0f) more efficiently.
    Include Guards: Add include guards to header files (#ifndef, #define, #endif) to prevent multiple inclusion.
    Use clock_gettime(): Instead of gettimeofday(), consider using clock_gettime() for higher precision timing.
    Improve Help Message: Enhance the help message (-h option) to provide more detailed information about the program's usage and available options.
    Use const Where Applicable: Declare variables as const where their values should not be modified.
    Avoid Magic Numbers: Replace magic numbers (e.g., 1000000, 1.0f) with named constants to improve code clarity and maintainability.
    Documentation: Add comments and documentation to explain the purpose and functionality of the code, especially for complex or non-obvious parts.

Applying these improvements can lead to a more efficient, maintainable, and readable codebase.
I: Pplease provide the code with all of the twelve items in the list applied.
*/

#include <omp.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h> 

#define DEFAULT_SAMPLES 1000000
#define DEFAULT_THREADS 1
#define PI 3.14159265358979323846

extern char *optarg;

int main(int argc, char* argv[]) {
    
    int samples = DEFAULT_SAMPLES;
    int threads = DEFAULT_THREADS;
    int opt;
    while( (opt = getopt(argc, argv, "hs:t:") ) != -1) 
    { 
        switch(opt) 
        { 
            case 's': 
                samples = atoi(optarg);
                break;
            case 't': 
                threads = atoi(optarg);
                break;
            case 'h':
                printf("$ 3_4_a -s {# of samples, default: %d} -t {# of threads, default: %d}\n", DEFAULT_SAMPLES, DEFAULT_THREADS);
                return 0;
        } 
    } 

    if (samples <= 0 || threads <= 0) {
        printf("Error: Invalid sample size or thread count.\n");
        return 1;
    }

    srand( (unsigned int)time(NULL) );
 
    double exec_time;
    struct timespec time_start, time_end;    
    clock_gettime(CLOCK_MONOTONIC, &time_start);

    float circle = 0;

    #pragma omp parallel for num_threads(threads) reduction(+:circle)
    for (int i = 0; i < samples; i++) {
        float x = 2.0f * ((float)rand() / (float)RAND_MAX) - 1.0f;
        float y = 2.0f * ((float)rand() / (float)RAND_MAX) - 1.0f;
        if (x * x + y * y <= 1.0f) {
            circle += 1.0f;
        }
    }
    
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    exec_time = (time_end.tv_sec - time_start.tv_sec) * 1000.0;
    exec_time += (time_end.tv_nsec - time_start.tv_nsec) / 1000000.0;
    float approx_pi = (4.0f * circle / (float)samples);  // PI = 4 * (circle / samples)

    printf("PI is approximately: %.15f\n", approx_pi);
    printf("Execution time: %.3f ms\n -> %d thread(s) and %d samples used.\n", exec_time, threads, samples);
    printf(" -> Error: %.15f\n", fabs(PI - approx_pi));

    return 0;
}
